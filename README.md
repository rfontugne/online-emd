# Online EMD #


### What is this repository for? ###

* This is the MATLAB implementation of Online EMD
* Version 0.1


### How do I get set up? ###

* Download the code here: https://bitbucket.org/rfontugne/online-emd/downloads
* You also need to install the EMD code from: http://perso.ens-lyon.fr/patrick.flandrin/emd.html
* This code requires no toolbox and should run out-of-the-box
* To understand how Online EMD works run and look at the example: example_oemd_fig2.m

### Who do I talk to? ###

* Romain Fontugne (http://romain.fontugne.free.fr)